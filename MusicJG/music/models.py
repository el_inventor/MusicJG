# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class Album(models.Model):
    # Unable to inspect table 'Album'
    # The error was: 'NoneType' object has no attribute 'groups'
    albumid =  models.IntegerField(db_column='AlbumId', primary_key=True)
    title = models.TextField(db_column='Title', blank=True, null=True, max_length=160)
    artistid = models.ForeignKey('Artist', db_column='ArtistId')
    class Meta:
        managed = False
        db_table = 'Album'


class Artist(models.Model):
    artistid = models.IntegerField(db_column='ArtistId', primary_key=True)  # Field name made lowercase.
    name = models.TextField(db_column='Name', blank=True, null=True)  # Field name made lowercase. This field type is a guess.

    class Meta:
        managed = False
        db_table = 'Artist'


# Unable to inspect table 'Customer'
# The error was: 'NoneType' object has no attribute 'groups'
# Unable to inspect table 'Employee'
# The error was: 'NoneType' object has no attribute 'groups'


class Genre(models.Model):
    genreid = models.IntegerField(db_column='GenreId', primary_key=True)  # Field name made lowercase.
    name = models.TextField(db_column='Name', blank=True, null=True)  # Field name made lowercase. This field type is a guess.

    class Meta:
        managed = False
        db_table = 'Genre'
# Unable to inspect table 'Invoice'
# The error was: 'NoneType' object has no attribute 'groups'
# Unable to inspect table 'InvoiceLine'
# The error was: 'NoneType' object has no attribute 'groups'


class Mediatype(models.Model):
    mediatypeid = models.IntegerField(db_column='MediaTypeId', primary_key=True)  # Field name made lowercase.
    name = models.TextField(db_column='Name', blank=True, null=True)  # Field name made lowercase. This field type is a guess.

    class Meta:
        managed = False
        db_table = 'MediaType'


class Playlist(models.Model):
    playlistid = models.IntegerField(db_column='PlaylistId', primary_key=True)  # Field name made lowercase.
    name = models.TextField(db_column='Name', blank=True, null=True)  # Field name made lowercase. This field type is a guess.

    class Meta:
        managed = False
        db_table = 'Playlist'

class PlaylistTrack(models.Model):
    # Unable to inspect table 'PlaylistTrack'
    # The error was: 'NoneType' object has no attribute 'groups'
    playlistid = models.ForeignKey('Playlist', db_column='PlaylistId')
    trackid = models.ForeignKey('Track', db_column='TrackId')
    class Meta:
        managed = False
        db_table = 'PlaylistTrack'


class Track(models.Model):
    # Unable to inspect table 'Track'
    # The error was: 'NoneType' object has no attribute 'groups'
    trackid = models.IntegerField(db_column='TrackId', primary_key=True)
    name = models.TextField(db_column='Name', blank=True)
    albumid = models.ForeignKey('Album', db_column='AlbumId')
    mediatypeid = models.ForeignKey('MediaType', db_column='MediaTypeId')
    genreid = models.ForeignKey('Genre', db_column='GenreId')
    composer = models.TextField(db_column='Composer', blank=True, null=True, max_length=200)
    milliseconds = models.IntegerField(db_column='Milliseconds')
    bytes = models.IntegerField(db_column='Bytes')
    unitprice = models.DecimalField(db_column='UnitPrice', max_digits=10, decimal_places=2)

    class Meta:
        managed = False
        db_table = 'Track'