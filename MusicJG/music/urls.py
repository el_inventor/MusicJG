# -*- conding:utf-8 -*-
from django.conf.urls import url
from django.conf.urls import include

from music.views import Albums
from music.views import Genres
from music.views import TrackByGenres
from music.views import Artists
from music.views import TrackByArtist
from music.views import AlbumByArtist
from music.views import PlayList
from music.views import AlbumByPlaylist
from music.views import Search



urlpatterns = [
    url(r'^$', Genres.as_view(), name='index'),
    url(r'^genres/', Genres.as_view(), name='genres'),
    url(r'^tracks_by_genre/(?P<genreid>\w+)?$', TrackByGenres.as_view(), name='track_by_genres'),
    url(r'^artists/', Artists.as_view(), name='artists'),
    url(r'^track_by_artist/(?P<artistid>\w+)?$', TrackByArtist.as_view(), name='track_by_artist'),
    url(r'^album_by_artist/(?P<albumid>\w+)?$', AlbumByArtist.as_view(), name='album_by_artist'),
    url(r'^playlist/', PlayList.as_view(), name='playlist'),
    url(r'^track_by_playlist/(?P<playlistid>\w+)?$', AlbumByPlaylist.as_view(), name='track_by_playlist'),
    url(r'^search/', Search.as_view(), name='search'),
    url(r'^albums/', Albums.as_view(), name='albums'),


]

