from django import template

register = template.Library()

@register.filter(name='menu_activo')

def menu_activo(request, nombre_menu): # Only one argument.
    clase = ''
        
    if request.path.__contains__(nombre_menu,):
        clase = 'active'        

    return clase