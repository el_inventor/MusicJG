# -*- coding:utf-8 -*-
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render
from django.db.models import Avg
from django.db.models import Max
from django.views.generic import View
from django.views.generic.base import ContextMixin
from music.models import Genre, Track, Artist, Album, Playlist


class Genres(View):
    '''
    Vista para consultar el listado de generos
    '''
    def get(self, request, *args, **kwargs):
        # En el index deberían mostrarse las canciones en el top
        generos = Genre.objects.all()

        paginator = Paginator(generos, 15)
        page = request.GET.get('page')

        try:
            genres = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            genres = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of
            # results.
            genres = paginator.page(paginator.num_pages)

        context = {'generos': genres}
        return render(request, 'genres.html', context)


class TrackByGenres(View):
    '''
    Vista para consultar el listado de traks por generos
    Retorna el listado de los tracks y los datos para las estadisticas
    '''
    def get(self, request, *args, **kwargs):
        tracks = Track.objects.filter(genreid=int(kwargs['genreid']))
        # Lista de ID de los artistas
        artists_by_genre = Artist.objects.filter(
            album__track__genreid_id=kwargs['genreid']).distinct()
        averages = []
        for artist in artists_by_genre:
            milliseconds_sum = Track.objects.filter(
                genreid_id=kwargs['genreid'],
                albumid__artistid=artist).aggregate(
                    prom_milli=Avg('milliseconds')
            )
            averages.append({'id': artist.pk, 'name': artist.name,
                             'milliseconds': milliseconds_sum['prom_milli']})

        max_milliseconds = Track.objects.filter(
            genreid_id=kwargs['genreid']).aggregate(Max('milliseconds'))

        paginator = Paginator(tracks, 25)
        page = request.GET.get('page')
        try:
            p_tracks = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            p_tracks = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of
            # results.
            p_tracks = paginator.page(paginator.num_pages)

        context = {
            'tracks': p_tracks,
            'averages': averages,
            'max_milliseconds': max_milliseconds
        }

        return render(request, 'tracks_by_genre.html', context)


class Artists(View):
    '''
    Vista para consultar el listado de artistas
    '''
    def get(self, request, *args, **kwargs):
        # En el index deberían mostrarse las canciones en el top
        artists = Artist.objects.all()

        paginator = Paginator(artists, 15)
        page = request.GET.get('page')
        try:
            artistas = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            artistas = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of
            # results.
            artistas = paginator.page(paginator.num_pages)

        context = {'artists': artistas}

        return render(request, 'artists.html', context)


class TrackByArtist(View):
    '''
    Vista para consultar el listado de tracks por artista
    '''
    def get(self, request, *args, **kwargs):
        tracks = Track.objects.filter(
            albumid__artistid=int(kwargs['artistid']))
        albums = Album.objects.filter(artistid=int(kwargs['artistid']))

        paginator = Paginator(tracks, 15)
        page = request.GET.get('page')
        try:
            p_tracks = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            p_tracks = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of
            # results.
            p_tracks = paginator.page(paginator.num_pages)

        context = {
            'tracks': p_tracks,
            'albums': albums,
        }

        return render(request, 'tracks_by_artist.html', context)


class AlbumByArtist(View):
    '''
    Vista para consultar el listado de Albumes por 
    artista seleccionado
    '''
    def get(self, request, *args, **kwargs):
        tracks = Track.objects.filter(albumid=int(kwargs['albumid']))

        paginator = Paginator(tracks, 15)
        page = request.GET.get('page')
        try:
            p_tracks = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            p_tracks = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of
            # results.
            p_tracks = paginator.page(paginator.num_pages)

        context = {
            'tracks': p_tracks,
        }

        return render(request, 'album_by_artist.html', context)


class PlayList(View):
    '''
    Vista para consultar el listado de los Playlist existentes
    '''
    def get(self, request, *args, **kwargs):
        playlists = Playlist.objects.all()
        paginator = Paginator(playlists, 15)
        page = request.GET.get('page')
        try:
            p_playlist = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            p_playlist = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of
            # results.
            p_playlist = paginator.page(paginator.num_pages)

        context = {'playlists': p_playlist}
        return render(request, 'playlist.html', context)


class AlbumByPlaylist(View):
    '''
    Vista para consultar el listado de los tracks y
    albumes que existen registrados en cada playlist
    '''
    def get(self, request, *args, **kwargs):
        tracks = Track.objects.filter(
            playlisttrack__playlistid=int(kwargs['playlistid']))
        other_playlists = Playlist.objects.all().exclude(
            pk=int(kwargs['playlistid']))

        paginator = Paginator(tracks, 15)
        page = request.GET.get('page')
        try:
            p_playlist = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            p_playlist = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of
            # results.
            p_playlist = paginator.page(paginator.num_pages)

        context = {
            'playlist_now': Playlist.objects.get(pk=int(kwargs['playlistid'])),
            'tracks': p_playlist,
            'other_playlists': other_playlists
        }

        return render(request, 'tracks_by_playlist.html', context)


class Search(View):
    '''
    Vista del buscador general del sitio
    recube la busqueda del formulario y retorna
    cualquier objeto (genero, track, album o playlist)
    que conincida con la búsqueda
    '''
    def post(self, request, *args, **kwargs):
        if ('busqueda' in request.POST.keys() and request.POST.get('busqueda') is not ''):
            genres = Genre.objects.filter(
                name__icontains=request.POST.get('busqueda'))
            tracks = Track.objects.filter(
                name__icontains=request.POST.get('busqueda'))
            artists = Artist.objects.filter(
                name__icontains=request.POST.get('busqueda'))
            albums = Album.objects.filter(
                title__icontains=request.POST.get('busqueda'))
            playlists = Playlist.objects.filter(
                name__icontains=request.POST.get('busqueda'))

            context = {
                'generos': genres,
                'tracks': tracks,
                'albums': albums,
                'playlists': playlists
            }

        return render(request, 'search.html', context)


class Albums(View):
    '''
    Vista para consultar el listado de albumes
    '''
    def get(self, request, *args, **kwargs):
        albums = Album.objects.all()
        paginator = Paginator(albums, 15)
        page = request.GET.get('page')
        try:
            p_album = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            p_album = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of
            # results.
            p_album = paginator.page(paginator.num_pages)

        context = {'albums': p_album}
        return render(request, 'albums.html', context)
