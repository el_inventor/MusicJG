=========================
   1) Instalar (en Linux)
=========================

  Preparar ambiente (Instalar pip, vitualenv y Djnago)

=====

  **Instalar pip**

------

Instalar PIP en Debian haremos:

Nos logueamos como root y tecleamos:

**Nota:** el “#” no se coloca, es sólo para identificar que estamos com root.
::

  # aptitude install python-pip python-dev build-essential
  # pip install --upgrade pip

En Ubuntu:
::

  sudo apt-get install python-setuptools python-dev build-essential
  sudo easy_install pip

**Nota para ubuntu:** esta que describo aquí es la recomendada, pero tambien sirve la manera de debian. Si por alguna razón no les llegara a funcionar este método, pueden probar:

::

  sudo aptitude install python-pip python-dev build-essential
  sudo pip install --upgrade pip

Con esto ya tenemos instalado pip

Una vez instalado PIP en Debian o Ubuntu

  **Instalar virtualenv:**

------


Para Debian:

::

  # pip install --upgrade virtualenv

Para Ubuntu:

::

  sudo pip install --upgrade virtualenv

Con esto, ya tenemos preparado nuestro entorno virtual.


  **Crear el entorno virtual**

------

Para crearlo me sitúo en directorio del proyecto:
::

  cd MusicJG

Creo el entorno virtual:
::

  virtualenv env

Esto me creará un directorio “env” que es mi entorno virtual.

Activo mi virtualenv:

::

 source env/bin/activate

y aparecerá algo así

::

(env)usuario@usuario$

eso me indica que ya estoy en mi entorno virtual.



 **Instalar requirements.txt**

------

Lo que tenemos que hacer es situarnos(por una terminal) en el directorio del proyecto  y simplemente se ejecuta el siguiente comando:

::

  cd MusicJG

Y luego 

::

  pip install -r requirements.txt

Es importante el -r porque el install lee el archivo requirements.txt linea por linea y si no tiene este parametro sólo leerá la primera y obviamente causará problemas.

**Nota:** Si estás trabajando en un virtualenv debes primero activar el entorno virtual (el virtualevn/bin/activate…).

Haciendo esto, se instalaran en el sistema o en el virtualenv que tengamos activado, los paquetes anotados en el archivo.


===============================
   2) Cómo correr la aplicación
===============================

Ingresar al directorio de la aplicación:

::

  cd MusicJG

Correr el servidor de la aplicación:

::

  python manage.py runserver

Laterminal mostrará algo como esto:

::

  March 27, 2017 - 07:28:10
  Django version 1.10.6, using settings 'MusicJG.settings'
  Starting development server at http://127.0.0.1:8000/
  Quit the server with CONTROL-C.

Abrimos el siguiente enlace en un navegador web (Firefox, chrome, etc.)

::

  http://127.0.0.1:8000/
